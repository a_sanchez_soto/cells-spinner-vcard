import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  --bbva-spinner-size: 90px;
  @apply --cells-spinner-global-pfco; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

bbva-spinner {
  height: 100%;
  background: #fff;
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  z-index: 9001;
  opacity: 0.65; }

.overlay {
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 999;
  opacity: 0; }
`;