import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-spinner-vcard-styles.js';
import '@bbva-web-components/bbva-spinner';
/**
This component ...

Example:

```html
<cells-spinner-vcard></cells-spinner-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
export class CellsSpinnerVcard extends LitElement {
  static get is() {
    return 'cells-spinner-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      activeLoading: { type: Boolean }
    };
  }

  // Initialize properties
  constructor() {
    super();
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-spinner-vcard-shared-styles').cssText}
    `;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <bbva-spinner with-mask ?hidden="${!this.activeLoading}" ></bbva-spinner>
    ${this.activeLoading ? html `<div class="overlay"></div>` : '' }
    `;
  }
}

// Register the element with the browser
customElements.define(CellsSpinnerVcard.is, CellsSpinnerVcard);
