import { setDocumentCustomStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    font-family: var(--cells-fontDefault, sans-serif);
    margin: 0;
  }

  .button {
    color: #fff;
    background-color: #4CAF50;
    border: none;
    display: inline-block;
    padding: 8px 16px;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none;
    text-align: center;
    cursor: pointer;
    white-space: nowrap;
    position:absolute;
    top:0px;
    right:0px;
    z-index:100000;
  }
`);
